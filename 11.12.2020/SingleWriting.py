import pandas as pd
from pandas import DataFrame
from sklearn.neighbors import NearestNeighbors
import numpy as np
from numpy import concatenate
from typing import Union, List, Dict, Any, TYPE_CHECKING,Optional
from utils import _distinct_from_list, _copy_and_remove, _list_union, _get_default_mmc, ensure_rng, _make_xy, _Bay_Re

class SingleDataSet:

    def __init__(self,
                 data: DataFrame,
                 variable_schema: Union[List[str], Dict[str, List[str]]] = None,
                 mean_match_candidates: Union[int, Dict[str, int]] = None,
                 save_all_iterations: bool = True,
                 save_models: int = 1,
                 random_state: Union[int, np.random.RandomState] = None,
                 ):

        # Data
        self.data = data

        # Variable Schema 插补对象（key）和用于插补的变量（value） 最终变为dict
        self.na_where = data.isnull()
        self.na_counts = self.na_where.sum()
        self.data_dtypes = data.dtypes
        self.data_shape = data.shape
        self.data_variables = list(data.columns)
        self.vars_with_any_missing = list(
            self.na_counts[self.na_counts > 0].keys())

        if variable_schema is None:
            variable_schema = self.vars_with_any_missing #插补对象list [1,3,4]

        if isinstance(variable_schema, list): #最终型式：1:[2,3,4]
            variable_schema = {
                var: _copy_and_remove(self.data_variables, [var])
                for var in variable_schema
            }

        if isinstance(variable_schema, dict): #检验是否自己插补自己，但是没有检验设置的variable schema是否都是有缺失值的
            self_impute_attempt = {
                key: (key in value) for key, value in variable_schema.items()
            }
            if any(self_impute_attempt.values()):
                self_impute_vars = [
                    key for key, value in self_impute_attempt.items() if value
                ]
                raise ValueError(
                    ",".join(self_impute_vars)
                    + " variables cannot be used to impute itself."
                )
        self.variable_schema = variable_schema

        # mean match candidates
        self.mean_match_candidates = mean_match_candidates
        self.response_vars = list(variable_schema) #插补的对象

        available_candidates = {
            var: self.data_shape[0] - self.data[var].isna().sum()
            for var in self.response_vars
        } #每行有多少个predictor可以用于插补

        if self.mean_match_candidates is None:
            self.mean_match_candidates = {
                var: _get_default_mmc(available_candidates[var])
                for var in self.response_vars
            }

        elif isinstance(self.mean_match_candidates, int):
            self.mean_match_candidates = {
                key: self.mean_match_candidates for key in self.response_vars
            }
        elif isinstance(mean_match_candidates, dict):
            if not set(mean_match_candidates) == set(self.response_vars):  #set()只保留keys
                raise ValueError(
                    "mean_match_candidates not consistent with variable_schema. "
                    + "Do all variables in variable_schema have missing values?."
                )

        mmc_inadequate = [
            var
            for var, mmc in self.mean_match_candidates.items()
            if (mmc >= available_candidates[var])
        ]
        if len(mmc_inadequate) > 0:
            raise ValueError(
                "<"
                + ",".join(mmc_inadequate)
                + ">"
                + " do not have enough candidates to perform mean matching."
            )




        #save all iterations
        self.save_all_iterations = save_all_iterations

        #save models
        self.save_models = save_models

        # random state
        self._random_state = ensure_rng(random_state)
        self.models: Dict[str, Dict] = {var: {0: None} for var in self.response_vars}



    def __repr__(self):

        attribute = f'''
        Method : SingleImputation
        Iterations: {self.iteration_count()} 
        Imputed Variables: {self.n_imputed_vars}
        save_all_iterations: {self.save_all_iterations}'''
        return attribute

    def __getitem__(self, tup):
        var, iteration = tup
        return self.imputation_values[var][iteration]

    def __setitem__(self, tup, newitem):
        var, iteration = tup
        self.imputation_values[var][iteration] = newitem

    def __delitem__(self, tup):
        var, iteration = tup
        del self.imputation_values[var][iteration]

    def iteration_count(self, var: str = None) -> int:

        self.n_imputed_vars = len(self.variable_schema)
        self.predictor_vars = _distinct_from_list(
            concatenate([value for key, value in self.variable_schema.items()])
        )

        self.all_vars = _distinct_from_list(
            self.response_vars + self.predictor_vars)

        self._all_imputed_vars = _list_union(
            self.vars_with_any_missing, self.all_vars)

        self.imputation_values: Dict[str, Dict] = {
            var: dict() for var in self._all_imputed_vars
        }
        for var in self._all_imputed_vars:
            self.imputation_values[var] = {
                0: self._random_state.choice(
                    self.data[var].dropna(), size=self.na_counts[var]
                )
            }

        if var is None:
            var_iterations = [
                np.max(list(itr))
                for var, itr in self.imputation_values.items()
                if var in self.response_vars
            ]
            distinct_iterations = _distinct_from_list(var_iterations)
            if len(distinct_iterations) > 1:
                raise ValueError(
                    "Inconsistent state - cannot get meta iteration count."
                )
            else:
                return next(iter(distinct_iterations))
        else:
            return np.max(list(self.imputation_values[var]))

    def _insert_new_data(self, var: str, new_data: np.ndarray):
        current_iter = self.iteration_count(var)
        if not self.save_all_iterations:
            del self[var, current_iter]
        self[var, current_iter + 1] = new_data



    def _make_xy(self, var: str, iteration: int = None):

        self.categorical_variables = list(
            self.data_dtypes[self.data_dtypes == "category"].keys()
        )

        xvars = self.variable_schema[var]
        completed_data = self.complete_data(iteration=iteration, all_vars=True)
        to_convert = _list_union(self.categorical_variables, xvars)
        for ctc in to_convert:
            completed_data[ctc] = completed_data[ctc].cat.codes
        x = completed_data[xvars]
        y = completed_data[var]
        return x, y

    def complete_data(self, iteration: int = None, all_vars: bool = False) -> DataFrame:

        imputed_dataframe = self.data.copy()
        ret_vars = self._all_imputed_vars if all_vars else self.response_vars

        for var in ret_vars:
            itrn = self._default_iteration(iteration=iteration, var=var)
            imputed_dataframe.loc[self.na_where[var], var] = self[var, itrn]
        return imputed_dataframe

    def _default_iteration(self, iteration: Optional[int], **kwargs) -> int:

        if iteration is None:
            return self.iteration_count()
        else:
            return iteration

    def _insert_new_model(self, var, model):

        current_iter = self.iteration_count(var)
        if self.save_models == 0:
            return
        else:
            self.models[var][current_iter + 1] = model
        if self.save_models == 1:
            del self.models[var][current_iter]

    def _mean_match(
        self,
        model,
        mmc: int,
        is_categorical: bool,
        bachelor_features: DataFrame,
        candidate_features: DataFrame = None,
        candidate_values: np.array = None,
    ):

        if is_categorical:

            bachelor_preds = model.predict_proba(bachelor_features)
            imp_values = [
                self._random_state.choice(model.classes_, p=p, size=1)[0]
                for p in bachelor_preds
            ]
        else:

            bachelor_preds = np.array(model.predict(bachelor_features))
            candidate_preds = np.array(model.predict(candidate_features))
            candidate_values = np.array(candidate_values)

            knn = NearestNeighbors(n_neighbors=mmc, algorithm="ball_tree")
            knn.fit(candidate_preds.reshape(-1, 1))
            knn_indices = knn.kneighbors(
                bachelor_preds.reshape(-1, 1), return_distance=False
            )
            index_choice: List[int] = [
                self._random_state.choice(i) for i in knn_indices
            ]
            imp_values = candidate_values[index_choice]

        return imp_values




    def mice(self, iterations: int = 5, verbose: bool = False, **kw_fit):


        iterations_at_start = self.iteration_count()
        iter_range = range(
            iterations_at_start + 1, iterations_at_start + iterations + 1
        )

        assert isinstance(self.mean_match_candidates, Dict)

        for iteration in iter_range:
            if verbose:
                print(str(iteration) + " ", end="")
            for var in self.response_vars:
                if verbose:
                    print(" | " + var, end="")

                x, y = self._make_xy(var=var)
                non_missing_ind = self.na_where[var] == False
                candidate_features = x[non_missing_ind]
                candidate_values = y[non_missing_ind]

                current_model = _Bay_Re(**kw_fit)
                current_model.fit(X=candidate_features, y=candidate_values)

                #self._insert_new_model(var=var, model=current_model)

                bachelor_features = x[self.na_where[var]]
                mmc = self.mean_match_candidates[var]

                if mmc == 0:

                    imp_values = current_model.predict(bachelor_features)

                else:
                    is_categorical = var in self.categorical_variables
                    if is_categorical:
                        candidate_features = candidate_values = None
                    else:
                        ind = self.na_where[var] == False
                        candidate_features = x[ind]
                        candidate_values = y[ind]

                    imp_values = self._mean_match(
                        model=current_model,
                        mmc=mmc,
                        is_categorical=is_categorical,
                        bachelor_features=bachelor_features,
                        candidate_features=candidate_features,
                        candidate_values=candidate_values,
                    )

                self._insert_new_data(var, imp_values)

            if verbose:
                print("\n", end="")

    def complete_data(self, iteration: int = None, all_vars: bool = False) -> DataFrame:

        imputed_dataframe = self.data.copy()

        ret_vars = self._all_imputed_vars if all_vars else self.response_vars

        for var in ret_vars:
            itrn = self._default_iteration(iteration=iteration, var=var)
            imputed_dataframe.loc[self.na_where[var], var] = self[var, itrn]
        return imputed_dataframe













