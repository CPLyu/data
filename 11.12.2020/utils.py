import numpy as np
from typing import List, Optional, Union, Any, TYPE_CHECKING
from sklearn.linear_model import BayesianRidge
from sklearn import linear_model

def _distinct_from_list(
    lst: Union[np.ndarray, List[Any]], flatten: bool = False
) -> List[Any]:
    output = []

    if flatten:
        lst = [item for sublist in lst for item in sublist]

    for item in lst:
        if item not in output:
            output.append(item)
    return output


def _copy_and_remove(lst, elements):
    lt = lst.copy()
    for element in elements:
        lt.remove(element)
    return lt

def _list_union(a, b):
    return [element for element in a if element in b]

def _get_default_mmc(candidates=None):
    if candidates is None:
        return 5
    else:
        percent = 0.001
        minimum = 5
        mean_match_candidates = max(minimum, int(percent * candidates))
        return mean_match_candidates

def ensure_rng(
    random_state: Optional[Union[int, np.random.RandomState]] = None
) -> np.random.RandomState:

    if random_state is None:
        random_state = np.random.RandomState()
    elif isinstance(random_state, int):
        random_state = np.random.RandomState(random_state)
    else:
        assert isinstance(random_state, np.random.RandomState)
    return random_state

def _make_xy(self, var: str, iteration: int = None):

        xvars = self.variable_schema[var]
        completed_data = self.complete_data(iteration=iteration, all_vars=True)
        to_convert = _list_union(self.categorical_variables, xvars)
        for ctc in to_convert:
            completed_data[ctc] = completed_data[ctc].cat.codes
        x = completed_data[xvars]
        y = completed_data[var]
        return x, y

def _Bay_Re(
            n_iter = 50,
            tol = 1e-3,
            alpha_1 = 1e-6,
            alpha_2 = 1e-6,
            lambda_1 = 1e-6,
            lambda_2 = 1e-6,
            alpha_init = None,
            lambda_init = None,
            compute_score = False,
            fit_intercept = True,
            normalize = True,
            copy_X = True,
            verbose = False,
            **kw_fit
    ) -> BayesianRidge:

        BayRi = linear_model.BayesianRidge(
            n_iter = n_iter,
            tol = tol,
            alpha_1 = alpha_1,
            alpha_2 = alpha_2,
            lambda_1 = lambda_1,
            lambda_2 = lambda_2,
            alpha_init = alpha_init,
            lambda_init = lambda_init,
            compute_score = compute_score,
            fit_intercept = fit_intercept,
            normalize = normalize,
            copy_X = copy_X,
            verbose = verbose,
            **kw_fit
            )
        return BayRi

